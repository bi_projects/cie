class Registry::ReportsController < Registry::AuthorizeController
  #before_action :set_registry_report, only: [:show, :edit, :update, :destroy]
  skip_load_and_authorize_resource
  authorize_resource :class => false

  layout "registry/dashboard_layout"
  # GET /registry/reports
  # GET /registry/reports.json
  def index
    redirect_to registry_root_path
  end

  # GET /registry/reports/1
  # GET /registry/reports/1.json
  def show
    redirect_to registry_reports_url
  end

  # GET /registry/reports/new
  def new
    redirect_to registry_reports_url
  end

  # GET /registry/reports/1/edit
  def edit
    redirect_to registry_reports_url
  end

  # POST /registry/reports
  # POST /registry/reports.json
  def create
   redirect_to registry_reports_url
  end

  # PATCH/PUT /registry/reports/1
  # PATCH/PUT /registry/reports/1.json
  def update
    redirect_to registry_reports_url
  end

  # DELETE /registry/reports/1
  # DELETE /registry/reports/1.json
  def destroy
    redirect_to registry_reports_url
  end

  def filtering_params(params, symbol)
    if params[:filter].present? && params[:filter][symbol].present?
      Registry::Event.filter(params[:filter].slice(symbol))
    else
      false
    end
  end

  def group_week_events
    
  end

  def employee_month_events
  end
  
  def employee_week_events
  end
  
  def patient_week_events
  end
  
  def room_week_events
  end
  
  def assistance_format
  end

  def group_week_hours
  end

  def group_week_covered_hours
  end


  def print_report


    @coordinador = Registry::Employee.where(:registry_employee_position_id => Registry::EmployeePosition.where(name: "Coordinador").pluck(:id))
    @employees_coordinador = Registry::Employee.where(boss_id: @coordinador)

    @registry_events = filtering_params(params, params[:filter][:symbol].to_sym)

    @registry_events = Registry::Event.where("registry_employee_id in (?) or registry_employee_id in (?) ", @coordinador.select(:id).uniq, @employees_coordinador.select(:id).uniq ) if params[:filter][:symbol].to_sym == :coordinador_covered_hour

    if @registry_events
      
      @start_date = params[:filter][:start_date]
      @end_date = params[:filter][:end_date]
      
      #@registry_events.where("date(start_time) between ? and ?", @start_date.to_date, @end_date.to_date)

      @calendar_registry_events = @registry_events.flat_map{ |e| e.calendar_events(params.fetch(:start_date, params[:filter][:start_date]).to_date, @end_date.to_date)}
      #filter like linq
      #@calendar_registry_events.select { |event| event.start_time >= @start_date.to_date && event.start_time <= @end_date.to_date }

      @report_scenario = Registry::TherapyGroup.find(params[:filter][:therapy_group]).display_name if params[:filter][:therapy_group].present?
      @report_scenario = Registry::Employee.find(params[:filter][:employee]).display_name if params[:filter][:employee].present?
      @report_scenario = Registry::Patient.find(params[:filter][:patient]).display_name if params[:filter][:patient].present?
      @report_scenario = Registry::TherapyRoom.find(params[:filter][:therapy_room]).display_name if params[:filter][:therapy_room].present?
      
      @report_type = :hour_report if params[:filter][:hour_report]

      @report_type = :hour_covered_report if params[:filter][:hour_covered_report]
      #@report_type = :therapy_group if params[:filter][:therapy_group].present?
      @report_type = :employee if params[:filter][:employee].present?
      
      @report_type = :coordinador_covered_report if params[:filter][:coordinador_covered_report]
      @report_scenario = "Consolidado Mensual Por Coordinador" if params[:filter][:coordinador_covered_report]
      if params[:filter][:patient].present?
        @patients = Array.new.push(params[:filter][:patient].to_i)
      end

      if params[:filter][:therapy_room].present?
        @rooms = Array.new.push(params[:filter][:therapy_room].to_i)
      end

      if params[:filter][:period].present? && params[:filter][:period].to_sym == :month
        @report_type = :employee_month
      end

      @single_report = true if params[:filter][:single].present?
      @employees = @registry_events.pluck(:registry_employee_id).uniq

      if @report_type == :coordinador_covered_report
        @single_report = true
        @employees = Registry::Employee.where(:id => @registry_events.pluck(:registry_employee_id).uniq)
      end

      @utilities = Utilities::Utilities.new
      respond_to do |format|
        format.pdf do
          if @single_report
            render pdf: "#{@report_scenario}_agenda_"+Time.now.to_s, orientation: 'Landscape', disposition:  'attachment', footer: { right: 'Pagina [page] de [topage]' }
          else 
            render pdf: "#{@report_scenario}_agenda_"+Time.now.to_s, orientation: 'Portrait', disposition:  'attachment', footer: { right: 'Pagina [page] de [topage]' }
          end
        end
      end
    else
      respond_to do |format|
        format.pdf do
          render pdf: "#{@report_scenario}_agenda_"+Time.now.to_s, orientation: 'Landscape', disposition:  'attachment', footer: { right: 'Pagina [page] de [topage]' }

        end
      end
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    #def set_registry_report
    #  @registry_report = Registry::Report.find(params[:id])
    #end

    # Never trust parameters from the scary internet, only allow the white list through.
    #def registry_report_params
    #  params.fetch(:registry_report, {})
    #end
end
