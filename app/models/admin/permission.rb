class Admin::Permission < ApplicationRecord
  belongs_to :admin_action, :class_name => "Admin::Action"
  belongs_to :admin_resource, :class_name => "Admin::Resource", :inverse_of => :admin_permissions
end
