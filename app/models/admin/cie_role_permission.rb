class Admin::CieRolePermission < ApplicationRecord
	belongs_to :admin_resource, :class_name => "Admin::Resource"
	belongs_to :admin_cie_role, :class_name => "Admin::CieRole"
	belongs_to :admin_action, :class_name => "Admin::Action"
	
	attr_accessor :permission_validator

  	accepts_nested_attributes_for :admin_cie_role,
		:allow_destroy => true,
		:reject_if     => :all_blank
end
