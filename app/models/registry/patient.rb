class Registry::Patient < ApplicationRecord
	mount_uploader :child_avatar, AvatarUploader
	before_destroy :check_for_event
	
	belongs_to :registry_department, :class_name => "Registry::Department"
	belongs_to :registry_municipality, :class_name => "Registry::Municipality"

	belongs_to :registry_condition, :class_name => "Registry::Condition", :inverse_of => :registry_patients

	has_many :registry_event_patients, :class_name => "Registry::EventPatient", 
		foreign_key: "registry_patient_id", :inverse_of => :registry_patient

	has_many :registry_events, :through => :registry_event_patients, :class_name => "Registry::Event"

	has_one :registry_treatment_requirement, :class_name => "Registry::TreatmentRequirement", 
		foreign_key: "registry_patient_id", :inverse_of => :registry_patient, :dependent => :destroy

	has_many :registry_medical_records, :class_name => "Registry::MedicalRecord", 
		foreign_key: "registry_patient_id", :inverse_of => :registry_patient, :dependent => :destroy

	has_many :registry_date_events, :class_name => "Registry::DateEvent", 
		foreign_key: "registry_patient_id", :inverse_of => :registry_patient

	has_many :registry_patient_assistances, :class_name => "Registry::PatientAssistance", 
		foreign_key: "registry_patient_id", :inverse_of => :registry_patient, :dependent => :destroy

	accepts_nested_attributes_for :registry_patient_assistances,
		:allow_destroy => true,
		:reject_if     => :all_blank
		
	accepts_nested_attributes_for :registry_treatment_requirement,
		:allow_destroy => true,
		:reject_if     => :all_blank
		
	accepts_nested_attributes_for :registry_medical_records,
		:allow_destroy => true,
		:reject_if     => :all_blank

	scope :patient, -> (patient) { where(:id => patient)}
	scope :condition, -> (condition) { where(:registry_condition => condition)}
	scope :gender, -> (gender) { where(:gender => gender)}
	scope :department, -> (department) { where(:registry_department => department)}
	scope :status, -> (status) { status == "En Espera" ? where("patient_state is null") : where(:patient_state => status)}

	include AvoidDestroyReferencesUtility 
	include PersonalData 
	include Filterable
	include Utility
	include UserModelUtility
	
	validates_format_of :email, 
		:email => true,
		:with => /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i, 
		:unless => Proc.new {|c| c.email.blank?},
		:uniqueness => { :case_sensitive => false }
	
	validates_presence_of :gender, :registry_condition, :registry_municipality, :registry_department, 
	:reference_to_center, :address, :comments

	def self.gender_map
		[["Masculino", "Masculino"],
		 ["Femenino", "Femenino"]]
	end

	def self.status_map
		[["Activo", "Activo"],
		 ["No Activo", "No Activo"],
		 ["En Espera", "En Espera"],
		 ["En Proceso", "En Proceso"]]
	end

	def self.patient_type_map
		[["Privado","Privado"],["INSS","INSS"]]
	end

	def self.security_name
		"Pacientes"
	end
  	def birth_date_formatted
		if !self.new_record?
			birth_date.strftime("%d/%m/%Y")
		else
			birth_date
		end
	end

	def patient_type_map
		["Privado","INSS"]
	end

	def patient_state_map
		[["Activo", "Activo"],["No Activo", "No Activo"],
		 ["En Proceso", "En Proceso"]]
	end

	def start_medical_date_formatted
		if !self.new_record? && medical_start_date.present?
			medical_start_date.strftime("%d/%m/%Y")
		else
			medical_start_date
		end
	end

	def end_medical_date_formatted
		if !self.new_record? && medical_end_date.present?
			medical_end_date.strftime("%d/%m/%Y")
		else
			medical_end_date
		end
	end

  	def display_name
		"#{first_name} #{last_name}"
	end
	def hours_to_invoice(start_date, end_date, invoice_record, patient_hour_record)
		patient_hour_record[self.id.to_s][:total_hours] = 0
		assistances = self.registry_patient_assistances
			.where("registry_patient_assistances.assistance_date between ? and ?",
				Date.strptime(start_date, '%d/%m/%Y').to_s,
				Date.strptime(end_date, '%d/%m/%Y').to_s
			).order(:assistance_date)
		assistances.each do |assistance|
			(Date.strptime(start_date, '%d/%m/%Y') .. Date.strptime(end_date, '%d/%m/%Y')).each do |date|
				if invoice_record[self.id.to_s][date.strftime("%d/%m/%Y")].blank?
					invoice_record[self.id.to_s][date.strftime("%d/%m/%Y")] = {}
				end
				if assistance.assistance_date.strftime("%d/%m/%Y") == date.strftime("%d/%m/%Y") && assistance.scenario.to_sym == :Entrada
					invoice_record[self.id.to_s][date.strftime("%d/%m/%Y")][:entrada] = assistance.assistance_date
				elsif assistance.assistance_date.strftime("%d/%m/%Y") == date.strftime("%d/%m/%Y") && assistance.scenario.to_sym == :Salida
					invoice_record[self.id.to_s][date.strftime("%d/%m/%Y")][:salida] = assistance.assistance_date
				end

				if invoice_record[self.id.to_s][date.strftime("%d/%m/%Y")][:entrada].present? &&
					invoice_record[self.id.to_s][date.strftime("%d/%m/%Y")][:salida].present?
					entrada = invoice_record[self.id.to_s][date.strftime("%d/%m/%Y")][:entrada].to_s
					salida = invoice_record[self.id.to_s][date.strftime("%d/%m/%Y")][:salida].to_s
					invoice_record[self.id.to_s][date.strftime("%d/%m/%Y")][:horas] = ((Time.parse(entrada) - Time.parse(salida))/3600).abs
				else 
					invoice_record[self.id.to_s][date.strftime("%d/%m/%Y")][:horas] = 0
				end
			end
		end
		invoice_record[self.id.to_s].each do |hour_by_date|
			patient_hour_record[self.id.to_s][:total_hours] += hour_by_date.last.present? ? hour_by_date.last[:horas] : 0
		end 
	end

	def hours_to_invoice_with_therapy(start_date, end_date, therapy_id, calendar_registry_events)

		start = Date.new.beginning_of_day + 7.hours
		ending = Date.new.end_of_day - 6.hours
		invoice_record = {}
		registered_hours = {}
		therapy_hour = 0
		assistances = self.registry_patient_assistances
			.where("registry_patient_assistances.assistance_date between ? and ?",
				Date.strptime(start_date, '%d/%m/%Y').to_s,
				Date.strptime(end_date, '%d/%m/%Y').to_s
			).order(:assistance_date, :scenario)
		
		array_date_range = (Date.strptime(start_date, '%d/%m/%Y') .. Date.strptime(end_date, '%d/%m/%Y')).to_a

		assistances.each do |assistance|
						
			array_date_range.each do |date|
				
				if invoice_record[date.strftime("%d/%m/%Y")].blank?
					invoice_record[date.strftime("%d/%m/%Y")] = {}
				end

				if assistance.assistance_date.strftime("%d/%m/%Y") == date.strftime("%d/%m/%Y") && assistance.scenario.to_sym == :Entrada

					invoice_record[date.strftime("%d/%m/%Y")][:entrada] = assistance.assistance_date

				elsif assistance.assistance_date.strftime("%d/%m/%Y") == date.strftime("%d/%m/%Y") && assistance.scenario.to_sym == :Salida

					invoice_record[date.strftime("%d/%m/%Y")][:salida] = assistance.assistance_date

				end

			end
		end
		if invoice_record.present?
			array_date_range.each_slice(5) do |week|
	            week.each_with_index do |day, index| 
					while start < ending do 
						calendar_registry_events.each do |event| 

							if event.dummy_start_time.hour == start.hour && event.start_time.to_date == day && 
						    	event.registry_therapy_room.registry_therapy_type_id == therapy_id

						    	if invoice_record[day.strftime("%d/%m/%Y")][:entrada].present? && invoice_record[day.strftime("%d/%m/%Y")][:salida].present?
									entrada = invoice_record[day.strftime("%d/%m/%Y")][:entrada].to_s
									salida = invoice_record[day.strftime("%d/%m/%Y")][:salida].to_s
									
									registered_hours[event.start_time.to_time] = false if registered_hours[event.start_time.to_time].blank?
									
									if event.start_time.to_time >= (Time.parse(entrada)) && event.start_time.to_time <= (Time.parse(salida)) && !registered_hours[event.start_time.to_time]

										registered_hours[event.start_time.to_time] = true
										therapy_hour += 1

									end
								end
						    end	
						end 
						start += 1.hour 
					end
					start = Date.new.beginning_of_day + 7.hours
	            end
		    end
		end
		therapy_hour
	end

	def in_charge_person
		name = ""
		if self.father_name.present? && self.mother_name.present?
			name = self.father_name + " / " + self.mother_name
		else
			if self.mother_name.present?
				name = mother_name
			end
			if self.father_name.present?
				name =father_name
			end
		end
		name
	end

	private
	def check_for_event
		check_for_relation(registry_events, "Evento")
	end
end
