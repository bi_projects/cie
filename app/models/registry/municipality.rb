class Registry::Municipality < ApplicationRecord
  belongs_to :registry_department, :class_name => "Registry::Department", :inverse_of => :registry_municipalities
  def display_name
  	self.name
  end
  def self.security_name
	"Municipio"
  end
end
