class Registry::TherapyGroupRoom < ApplicationRecord
  belongs_to :registry_therapy_group, :class_name => "Registry::TherapyGroup", :inverse_of => :registry_therapy_group_rooms
  belongs_to :registry_therapy_room, :class_name => "Registry::TherapyRoom", :inverse_of => :registry_therapy_group_rooms
  	def self.security_name
		"Salones de Terapia de Grupo"
	end
end
