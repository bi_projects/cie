class Registry::EventPatient < ApplicationRecord
  belongs_to :registry_event, :class_name => "Registry::Event", :inverse_of => :registry_event_patients
  belongs_to :registry_patient, :class_name => "Registry::Patient", :inverse_of => :registry_event_patients

  	def self.security_name
		"Agenda de Paciente"
	end
end