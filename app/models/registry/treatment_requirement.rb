class Registry::TreatmentRequirement < ApplicationRecord
  belongs_to :registry_patient, :class_name => "Registry::Patient", :inverse_of => :registry_treatment_requirement
  def self.security_name
    "Requerimientos"
  end
  def self.treatment_hash
  	{:entrance_letter => "Carta de solicitud", 
  		:epicrisis => "Epicrisis", 
  		:birth_certificate => "Partida de nacimiento", 
  		:beneficiary_child_license => "Carnet de beneficiario", 
  		:active_secured_parent_id_card => "Cedula de mamá o papá", 
  		:active_secured_parent_license => "Carnet de asegurado de mamá o papá", 
  		:payment_receipt_inss => "Comprobante de pago y derecho (Colilla del INSS)",
      :beneficiary_social_security_number => "Número INSS del beneficiario"
  	}
  end

end
