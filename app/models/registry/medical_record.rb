class Registry::MedicalRecord < ApplicationRecord
  belongs_to :registry_patient, :class_name => "Registry::Patient", :inverse_of => :registry_medical_records
  belongs_to :registry_therapy_type, :class_name => "Registry::TherapyType", :inverse_of => :registry_medical_records

  	def self.security_name
		"Ficha Clínica"
	end
end
