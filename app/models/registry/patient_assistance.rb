class Registry::PatientAssistance < ApplicationRecord
	belongs_to :registry_patient, :class_name => "Registry::Patient", :inverse_of => :registry_patient_assistances
	belongs_to :registry_employee, :class_name => "Registry::Employee", :inverse_of => :registry_patient_assistances

  	scope :employee, -> (employee) { where(:registry_employee => employee)}
  	scope :tutor, -> (tutor) { where("tutor like ?", "%"+tutor+"%")}
  	#scope :assist_date, -> (assist_date) { where("to_char(assistance_date, 'DD/MM/YYYY') = ?", assist_date)}
  	scope :scenario, -> (scenario) { where(:scenario => scenario)}
  	scope :operator_filter, -> (operator,assist_date) { where("assistance_date #{operator} to_date( ?, 'DD/MM/YYYY' )", assist_date)}

	scope :assistance_date, -> (start_date,end_date) { where("assistance_date between to_date( ?, 'DD/MM/YYYY' ) and to_date( ?, 'DD/MM/YYYY' )", start_date, end_date)}
	attr_accessor :date_range
	include Filterable
	validates_presence_of :tutor, :personal_code, :registry_employee_id, :assistance_date, :scenario
  	def start_time_formatted
  		if assistance_date.present?
			if !self.new_record?
				assistance_date.strftime("%d/%m/%Y %T")
			else
				assistance_date
			end
		else
			Time.now.strftime("%d/%m/%Y %T")
		end
	end
	def self.security_name
		"Asistencia"
	end
	def self.scenario_map
		[["Entrada", "Entrada"],
		 ["Salida", "Salida"]]
	end
end
