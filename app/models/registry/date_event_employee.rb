class Registry::DateEventEmployee < ApplicationRecord
  belongs_to :registry_date_event, :class_name => "Registry::DateEvent", :inverse_of => :registry_date_event_employees
  belongs_to :registry_employee, :class_name => "Registry::Employee", :inverse_of => :registry_date_event_employees
end
