class AddBossAndTherapyFieldsToRegistryEmployee < ActiveRecord::Migration[5.0]
  def change
    add_column :registry_employees, :boss_id, :integer
    add_column :registry_employees, :registry_therapy_group_id, :integer
  end
end
